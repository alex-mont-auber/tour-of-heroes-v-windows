import { Hero } from './hero';

export const HEROES: Hero[] = [
    {id: 11, name: 'Dr.Nice', powerDescription: 'Todo lo que pueda salir bien, estara bien'},
    {id: 12, name: 'Narco', powerDescription: 'Le encanta el blanco'},
    {id: 13, name: 'Bombasto', powerDescription: 'Este tio es la bomba!'},
    {id: 14, name: 'Celeritas', powerDescription: 'El correcaminos humano'},
    {id: 15, name: 'Magneta', powerDescription: 'Atrae lo intraible'},
    {id: 16, name: 'RubberMan', powerDescription: 'Este tio si lo chinchas, le encantara'},
    {id: 17, name: 'Dynama', powerDescription: 'Con mas voltios que una rama'},
    {id: 18, name: 'Dr IQ', powerDescription: 'Lo aprueba todo, fijate tu!'},
    {id: 19, name: 'Magma', powerDescription: 'Y lo tosta, y lo quema, y lo abrasaaa....asereje!'},
    {id: 20, name: 'Tornado', powerDescription: 'El tio nunca esta quieto!'}
];