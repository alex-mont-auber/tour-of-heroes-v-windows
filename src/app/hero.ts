export class Hero {
    id: number;
    name: string;
    powerDescription: string;
}