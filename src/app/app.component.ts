import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tour of Heroes';
  subtitle = 'Esta version esta creada a traves de el tutorial realizado en Angular.io. Los cambios realizados en el funcionamiento y/o diseño de la propia App son eleccion mia.';
}
